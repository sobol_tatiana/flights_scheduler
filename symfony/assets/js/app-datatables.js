var $ = require('jquery');
require('datatables.net');

var initializeDatatables = function () {
    $(".dataTable").DataTable({
        searching: false
    });

};

var initializeAirportsDatatable = function () {
    $("#airportsTable").dataTable({
        "ajax": "/airport/ajax",
        "columns": [
            {"data": "id"},
            {"data": "name"},
            {"data": "latitude"},
            {"data": "longitude"},
            {"data": "openTime"},
            {"data": "closeTime"},
            {
                mRender: function (data, type, row) {
                    var urlEdit = '/airport/' + row.id + '/edit';
                    var urlView = '/airport/' + row.id;
                    var linkEdit = '<a class="edit" href="' + urlView + '"><i class="fa fa-pen"></i></a>';
                    var linkView = '<a class="display" href="' + urlEdit + '"><i class="fa fa-eye"></i></a>';
                    return linkView + "  " + linkEdit;
                },
                sClass: "table-actions"
            }
        ],
        "processing": true,
        "serverSide": true,
        "ordering": false,
        "searching": false
    });
};

module.exports = function(){
    initializeDatatables();
    initializeAirportsDatatable();
};