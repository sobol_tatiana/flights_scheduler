
function getResultHtml(data){
    var html =
        '<div class="col-md-6 col-md-offset-3">'+
        '<div class="box box-solid">'+
        '<div class="box-body">'+
        '<div class="box-title">'+
        '<h4 class="box-title-text"> RESULT</h4>'+
        '</div>'+
        '<div class="media">' +
        '<span>Number of generated flights: ' + data.count + '</span><br/><br/>'+
        '<a href="/flight">Go to flights page </a>'+
        '</div>' +
        '</div>'+
        "</div>"+
        "</div>";
    return html;
}

$('#generateFlights').click(function () {
    $.ajax({
        url: "/flight/generate/go",
        success: function (data) {
            var content = getResultHtml(data);
            $('#generationResult').html(content);
        },
        error: function () {
            alert("error");
        }
    });
});