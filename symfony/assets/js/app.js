/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

require('../css/app.scss');

var $ = require('jquery');
window.$ = $;
window.jQuery = $;
require('bootstrap');
require('bootstrap-multiselect');
var initDatatables = require('./app-datatables');

$(document).ready(function () {
    initDatatables();
    initializeMultiselect();
});


var initializeMultiselect = function () {
    $('.multiselect').multiselect({
        maxHeight: 300,
        buttonContainer: '<div class="" style="width: 100%"/>',
        buttonWidth: '100%'
    });
};
