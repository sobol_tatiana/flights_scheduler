<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181212173805 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE airport (id INT AUTO_INCREMENT NOT NULL, city_id INT NOT NULL, name VARCHAR(255) NOT NULL, latitude NUMERIC(10, 8) NOT NULL, longitude NUMERIC(11, 8) NOT NULL, open_time TIME DEFAULT NULL, close_time TIME DEFAULT NULL, INDEX IDX_7E91F7C28BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flight (id INT AUTO_INCREMENT NOT NULL, airline_id INT NOT NULL, from_airport_id INT NOT NULL, to_airport_id INT NOT NULL, aiplane_id INT NOT NULL, take_off_time DATETIME NOT NULL, landing_time DATETIME NOT NULL, passengers INT NOT NULL, duration VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:dateinterval)\', distance NUMERIC(10, 5) NOT NULL, INDEX IDX_C257E60E130D0C16 (airline_id), INDEX IDX_C257E60E33B95CF (from_airport_id), INDEX IDX_C257E60EFACB1B5 (to_airport_id), INDEX IDX_C257E60EBD7B8906 (aiplane_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE airplane (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, distance NUMERIC(7, 2) NOT NULL, capacity SMALLINT NOT NULL, speed NUMERIC(7, 3) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE airport ADD CONSTRAINT FK_7E91F7C28BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E130D0C16 FOREIGN KEY (airline_id) REFERENCES airline (id)');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E33B95CF FOREIGN KEY (from_airport_id) REFERENCES airport (id)');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60EFACB1B5 FOREIGN KEY (to_airport_id) REFERENCES airport (id)');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60EBD7B8906 FOREIGN KEY (aiplane_id) REFERENCES airplane (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE flight DROP FOREIGN KEY FK_C257E60E33B95CF');
        $this->addSql('ALTER TABLE flight DROP FOREIGN KEY FK_C257E60EFACB1B5');
        $this->addSql('ALTER TABLE flight DROP FOREIGN KEY FK_C257E60EBD7B8906');
        $this->addSql('DROP TABLE airport');
        $this->addSql('DROP TABLE flight');
        $this->addSql('DROP TABLE airplane');
    }
}
