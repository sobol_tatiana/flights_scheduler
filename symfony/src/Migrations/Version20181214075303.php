<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181214075303 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE possession (id INT AUTO_INCREMENT NOT NULL, airline_id INT NOT NULL, airplane_id INT NOT NULL, number INT NOT NULL, INDEX IDX_F9EE3F42130D0C16 (airline_id), INDEX IDX_F9EE3F42996E853C (airplane_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE possession ADD CONSTRAINT FK_F9EE3F42130D0C16 FOREIGN KEY (airline_id) REFERENCES airline (id)');
        $this->addSql('ALTER TABLE possession ADD CONSTRAINT FK_F9EE3F42996E853C FOREIGN KEY (airplane_id) REFERENCES airplane (id)');
        $this->addSql('ALTER TABLE airport CHANGE open_time open_time TIME DEFAULT NULL, CHANGE close_time close_time TIME DEFAULT NULL');
        $this->addSql('ALTER TABLE flight CHANGE duration duration INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE possession');
        $this->addSql('ALTER TABLE airport CHANGE open_time open_time TIME DEFAULT \'NULL\', CHANGE close_time close_time TIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE flight CHANGE duration duration INT DEFAULT NULL');
    }
}
