<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181218101314 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE airplane_model (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, distance NUMERIC(7, 2) NOT NULL, capacity SMALLINT NOT NULL, speed NUMERIC(7, 3) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE possession');
        $this->addSql('ALTER TABLE airport CHANGE open_time open_time TIME DEFAULT NULL, CHANGE close_time close_time TIME DEFAULT NULL');
        $this->addSql('ALTER TABLE flight CHANGE duration duration INT DEFAULT NULL');
        $this->addSql('ALTER TABLE airplane ADD type_id INT NOT NULL, ADD airline_id INT NOT NULL, DROP distance, DROP capacity, DROP speed, CHANGE name code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE airplane ADD CONSTRAINT FK_2636002DC54C8C93 FOREIGN KEY (type_id) REFERENCES airplane_model (id)');
        $this->addSql('ALTER TABLE airplane ADD CONSTRAINT FK_2636002D130D0C16 FOREIGN KEY (airline_id) REFERENCES airline (id)');
        $this->addSql('CREATE INDEX IDX_2636002DC54C8C93 ON airplane (type_id)');
        $this->addSql('CREATE INDEX IDX_2636002D130D0C16 ON airplane (airline_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE airplane DROP FOREIGN KEY FK_2636002DC54C8C93');
        $this->addSql('CREATE TABLE possession (id INT AUTO_INCREMENT NOT NULL, airline_id INT NOT NULL, airplane_id INT NOT NULL, number INT DEFAULT 1 NOT NULL, INDEX IDX_F9EE3F42996E853C (airplane_id), INDEX IDX_F9EE3F42130D0C16 (airline_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE possession ADD CONSTRAINT FK_F9EE3F42130D0C16 FOREIGN KEY (airline_id) REFERENCES airline (id)');
        $this->addSql('ALTER TABLE possession ADD CONSTRAINT FK_F9EE3F42996E853C FOREIGN KEY (airplane_id) REFERENCES airplane (id)');
        $this->addSql('DROP TABLE airplane_model');
        $this->addSql('ALTER TABLE airplane DROP FOREIGN KEY FK_2636002D130D0C16');
        $this->addSql('DROP INDEX IDX_2636002DC54C8C93 ON airplane');
        $this->addSql('DROP INDEX IDX_2636002D130D0C16 ON airplane');
        $this->addSql('ALTER TABLE airplane ADD distance NUMERIC(7, 2) NOT NULL, ADD capacity SMALLINT NOT NULL, ADD speed NUMERIC(7, 3) NOT NULL, DROP type_id, DROP airline_id, CHANGE code name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE airport CHANGE open_time open_time TIME DEFAULT \'NULL\', CHANGE close_time close_time TIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE flight CHANGE duration duration INT DEFAULT NULL');
    }
}
