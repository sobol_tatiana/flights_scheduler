<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181217091331 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE airport CHANGE open_time open_time TIME DEFAULT NULL, CHANGE close_time close_time TIME DEFAULT NULL');
        $this->addSql('ALTER TABLE flight CHANGE duration duration INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX unique_airplane ON possession (airplane_id, airline_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE airport CHANGE open_time open_time TIME DEFAULT \'NULL\', CHANGE close_time close_time TIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE flight CHANGE duration duration INT DEFAULT NULL');
        $this->addSql('DROP INDEX unique_airplane ON possession');
    }
}
