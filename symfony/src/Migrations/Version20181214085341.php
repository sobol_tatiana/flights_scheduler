<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181214085341 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE airline_city (airline_id INT NOT NULL, city_id INT NOT NULL, INDEX IDX_5118FE53130D0C16 (airline_id), INDEX IDX_5118FE538BAC62AF (city_id), PRIMARY KEY(airline_id, city_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE airline_city ADD CONSTRAINT FK_5118FE53130D0C16 FOREIGN KEY (airline_id) REFERENCES airline (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE airline_city ADD CONSTRAINT FK_5118FE538BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE airport CHANGE open_time open_time TIME DEFAULT NULL, CHANGE close_time close_time TIME DEFAULT NULL');
        $this->addSql('ALTER TABLE flight CHANGE duration duration INT DEFAULT NULL');
        $this->addSql('ALTER TABLE airline ADD country_id INT NOT NULL');
        $this->addSql('ALTER TABLE airline ADD CONSTRAINT FK_EC141EF8F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('CREATE INDEX IDX_EC141EF8F92F3E70 ON airline (country_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE airline_city');
        $this->addSql('ALTER TABLE airline DROP FOREIGN KEY FK_EC141EF8F92F3E70');
        $this->addSql('DROP INDEX IDX_EC141EF8F92F3E70 ON airline');
        $this->addSql('ALTER TABLE airline DROP country_id');
        $this->addSql('ALTER TABLE airport CHANGE open_time open_time TIME DEFAULT \'NULL\', CHANGE close_time close_time TIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE flight CHANGE duration duration INT DEFAULT NULL');
    }
}
