<?php

namespace App\DataFixtures;

use App\Entity\AirplaneModel;
use App\Service\FixtureService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AirplaneModelFixture extends Fixture
{
    private $service;

    public function __construct(FixtureService $service)
    {
        $this->service = $service;
    }

    public function load(ObjectManager $manager)
    {
        $this->service->resetPrimaryKey(array('airplane_model'));
        $json = file_get_contents(__DIR__ . "/source/airplanes.json");
        $airplanesJsonData = json_decode($json, true);
        foreach ($airplanesJsonData as $element) {
            $airplane = new AirplaneModel();
            $airplane->setName($element['name']);
            $airplane->setCapacity($element['capacity']);
            $airplane->setDistance($element['distance']);
            $airplane->setSpeed($element['speed']);
            $manager->persist($airplane);
            $manager->flush();
        }
    }
}