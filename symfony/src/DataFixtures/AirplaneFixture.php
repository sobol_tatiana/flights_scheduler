<?php

namespace App\DataFixtures;

use App\Entity\Airline;
use App\Entity\Airplane;
use App\Entity\AirplaneModel;
use App\Service\FixtureService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AirplaneFixture extends Fixture implements DependentFixtureInterface
{

    private $service;

    public function __construct(FixtureService $service)
    {
        $this->service = $service;
    }

    public function load(ObjectManager $manager)
    {
        $this->service->resetPrimaryKey(array('airplane'));
        $airplanes = $manager->getRepository(AirplaneModel::class)->findAll();
        $airlines = $manager->getRepository(Airline::class)->findAll();

        foreach ($airlines as $airline) {
            $limit = rand(3, 20);
            $number = 0;
            while ($number < $limit) {
                $randomAirplane = $airplanes[array_rand($airplanes)];
                $this->addAirplaneToAirline($manager, $randomAirplane, $airline);
                $number++;
            }
        }
    }

    private function addAirplaneToAirline($manager, AirplaneModel $airplaneModel, Airline $airline)
    {
        $airplane = new Airplane();
        $airplane->setType($airplaneModel);
        $code = $this->generateCodeFromAirplaneType($airplaneModel->getName());
        $airplane->setCode($code);
        $airplane->setAirline($airline);
        $manager->persist($airplane);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(AirplaneModelFixture::class,
            AirlineFixture::class);
    }

    public function generateCodeFromAirplaneType($type)
    {
        $code = str_replace(' ', '', strtolower($type));
        $code .= $this->generateRandomString(15);
        return $code;
    }

    function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

}