<?php

namespace App\DataFixtures;

use App\Entity\Airport;
use App\Entity\City;
use App\Entity\Country;
use App\Service\FixtureService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AirportFixture extends Fixture implements DependentFixtureInterface
{
    private $service;

    public function __construct(FixtureService $service)
    {
        $this->service = $service;
    }

    public function load(ObjectManager $manager)
    {
        $this->service->resetPrimaryKey(array('airport', 'city'));
        $json = file_get_contents(__DIR__ . "/source/airports.json");
        $airports = json_decode($json, true);
        foreach ($airports as $element) {
            $country = $manager->getRepository(Country::class)
                ->findOneByName($element['country']);
            if ($country == null) continue;

            if (is_null($element['name'])) continue;

            $city = $manager->getRepository(City::class)
                ->findOneByName($element['city']);

            if (is_null($city)) {
                $city = $this->createCityFromAirportJsonData($manager, $element, $country);
            }

            $this->addAirport($manager, $element, $city);
        }
    }


    private function createCityFromAirportJsonData(ObjectManager $manager, $data, Country $country): City
    {
        $city = new City();
        $city->setName($data['city']);
        $city->setCountry($country);
        $manager->persist($city);
        $manager->flush();
        return $city;
    }

    private function addAirport(ObjectManager $manager, $data, $city)
    {
        $airport = new Airport();
        if(empty($data['name']) || empty($data['lat']) || empty($data['lon'])) {
            return;
        }
        $airport->setName($data['name']);
        $airport->setCity($city);
        $airport->setLatitude($data['lat']);
        $airport->setLongitude($data['lon']);
        $manager->persist($airport);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            CountryFixture::class,
        );
    }

}