<?php

namespace App\DataFixtures;

use App\Entity\Country;
use App\Service\FixtureService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CountryFixture extends Fixture
{
    private $service;

    public function __construct(FixtureService $service)
    {
        $this->service = $service;
    }

    public function load(ObjectManager $manager)
    {
        $this->service->resetPrimaryKey(array('country'));

        $countries = array(
            'AU' => 'Australia',
            'AT' => 'Austria',
            'BE' => 'Belgium',
            'BR' => 'Brazil',
            'CA' => 'Canada',
            'CN' => 'China',
            'CO' => 'Colombia',
            'CR' => 'Costa Rica',
            'HR' => 'Croatia',
            'CD' => 'Democratic Republic of the Congo',
            'DK' => 'Denmark',
            'EG' => 'Egypt',
            'FR' => 'France',
            'DE' => 'Germany',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'CI' => 'Ivory Coast',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'MX' => 'Mexico',
            'MD' => 'Moldova',
            'NL' => 'Netherlands',
            'NC' => 'New Caledonia',
            'RO' => 'Romania',
            'RU' => 'Russia',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'ZA' => 'South Africa',
            'KR' => 'South Korea',
            'ES' => 'Spain',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syria',
            'TR' => 'Turkey',
            'US' => 'United States'
        );

        foreach ($countries as $code => $name) {
            $country = new Country();
            $country->setName($name);
            $country->setCode($code);
            $manager->persist($country);
        }
        $manager->flush();
    }
}