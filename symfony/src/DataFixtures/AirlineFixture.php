<?php

namespace App\DataFixtures;

use App\Entity\Airline;
use App\Entity\City;
use App\Entity\Country;
use App\Service\FixtureService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AirlineFixture extends Fixture implements  DependentFixtureInterface
{
    const AIRLINE_MAX_OPERATED_CITIES = 30;
    const AIRLINE_MIN_OPERATED_CITIES = 10;

    private $service;

    public function __construct(FixtureService $service)
    {
        $this->service = $service;
    }

    public function load(ObjectManager $manager)
    {
        $this->service->resetPrimaryKey(array('airline', 'airline_city'));
        $json = file_get_contents(__DIR__ . "/source/airlines.json");
        $airlines = json_decode($json, true);
        $cities = $manager->getRepository(City::class)->findAll();
        foreach ($airlines as $element) {
            $country = $manager->getRepository(Country::class)
                ->findOneByCode($element['country']);
            if ($country == null) continue;

            $airline = new Airline();
            $airline->setName($element['name']);
            $airline->setCountry($country);
            $manager->persist($airline);

            $this->setOperatedCities($airline, $cities);
            $manager->persist($airline);
            $manager->flush();
        }
    }

    private function setOperatedCities($airline, $cities)
    {
        $country = $airline->getCountry();
        $innerCities = $country->getCities();
        foreach ($innerCities as $city) {
            $airline->addCity($city);
        }
        $numberOfOperatedCIties = rand(self::AIRLINE_MIN_OPERATED_CITIES, self::AIRLINE_MAX_OPERATED_CITIES);
        $rands = array_rand($cities, $numberOfOperatedCIties);
        foreach ($rands as $rand) {
            $outerCity = $cities[$rand];
            $airline->addCity($outerCity);
        }
    }

    public function getDependencies()
    {
        return array(
            CountryFixture::class,
            AirportFixture::class
        );
    }
}