<?php


namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class FixtureService
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function resetPrimaryKey($tablenames)
    {
        if(is_array($tablenames)){
            foreach($tablenames as $table){
                $sql = "ALTER TABLE $table AUTO_INCREMENT = 1";
                $query = $this->manager->getConnection()->prepare($sql);
                $query->execute();
            }
        }
    }

}
