<?php


namespace App\Service;


use App\Entity\Airline;
use App\Entity\Airplane;
use App\Entity\AirplaneModel;
use Doctrine\ORM\EntityManagerInterface;
//todo: add parameters type and return type
class LinkService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addAirplanesToAirline(Airline $airline, AirplaneModel $model, $number)
    {
        while ($number > 0) {
            $this->addAirplaneToAirline($airline, $model);
            $number--;
        }
    }

    public function addAirplaneToAirline(Airline $airline, AirplaneModel $model)
    {
        $airplane = new Airplane();
        $airplane->setType($model);
        $code = $this->generateCodeFromAirplaneType($model->getName());
        $airplane->setCode($code);
        $airplane->setAirline($airline);
        $this->entityManager->persist($airplane);
        $this->entityManager->flush();
    }

    private function generateCodeFromAirplaneType(string $type) : string
    {
        $code = str_replace(' ', '', strtolower($type));
        $code .= $this->generateRandomString(15);
        return $code;
    }

    function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

}