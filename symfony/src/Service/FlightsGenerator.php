<?php

namespace App\Service;

use App\Entity\Airline;
use App\Entity\Airplane;
use App\Entity\Airport;
use App\Entity\Flight;
use App\Repository\AirlineRepository;
use App\Repository\AirportRepository;
use App\Repository\FlightRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;

class FlightsGenerator
{
    private $manager;
    private $airportRepository;
    private $airlineRepository;
    private $flightRepository;
    private $formulas;
    private $fixture;
    private $selectedAirports;
    private $selectedAirlines;
    private $groupedAirports;
    private $distanceBetweenAirports;
    private $busyAirplanes;
    private $airplanesReservation;
    private $airlineDestinations;
    private $generatedFlights;

    const MIN_NB_AIRLINES = 2;
    const MAX_NB_AIRLINES = 5;
    const MAX_FLIGHTS_PER_AIRPLANE = 3;
    const MAX_NB_AIRPORTS = 10;
    const MAX_FLIGHTS_FOR_AIRPLANE = 3;
    const MIN_FLIGHTS_FOR_AIRPLANE = 2;
    const PASSENGERS_MAX_DEVIATION = 10;
    const MIN_DISTANCE_BETWEEN_AIRPORTS_AIRLINE = 200;//km
    const MIN_AIRPLANES_PER_AIRLINE = 3;
    const MAX_AIRPLANES_PER_AIRLINE = 20;
    const STR_DAY_FLIGHTS_GENERATOR = 'tomorrow midnight';
    const STR_NEXT_DAY_FLIGHTS_GENERATOR = 'tomorrow + 1day';
    const DATEINTERVAL_BETWEEN_AIRPLANE_FLIGHTS = 'PT1H';//1h interval

    /**
     * FlightsGenerator constructor.
     * @param EntityManagerInterface $manager
     * @param AirportRepository $airportRepository
     * @param AirlineRepository $airlineRepository
     * @param FlightRepository $flightRepository
     * @param FormulasService $formulas
     * @param FixtureService $fixture
     */
    public function __construct(EntityManagerInterface $manager, AirportRepository $airportRepository,
                                AirlineRepository $airlineRepository, FlightRepository $flightRepository,
                                FormulasService $formulas, FixtureService $fixture)
    {
        $this->manager = $manager;
        $this->airportRepository = $airportRepository;
        $this->airlineRepository = $airlineRepository;
        $this->flightRepository = $flightRepository;
        $this->formulas = $formulas;
        $this->fixture = $fixture;
        $this->generatedFlights = array();
        $this->airlineDestinations = array();
    }

    /**
     * @return array of Flight objects
     */
    public function generate()
    {
        $this->flightRepository->deleteAll();
        $this->fixture->resetPrimaryKey(array('flight'));

        $limitAirlines = rand(self::MIN_NB_AIRLINES, self::MAX_NB_AIRLINES);
        $this->selectedAirlines = $this->airlineRepository->findRandomAirlines($limitAirlines);

        $this->selectedAirports = $this->airportRepository->findTheMostOperatedForAirlines(self::MAX_NB_AIRPORTS, $this->selectedAirlines);
        $this->groupedAirports = $this->groupAirportsByAirline();
        $this->setDistanceBetweenSelectedAirports();

        foreach ($this->selectedAirlines as $airline) {
            if ($this->generateFlightsForAirline($airline) === false) continue;
        }
        $this->saveTheFlightsInDatabase();
        return $this->generatedFlights;
    }

    /**
     * Group airports ids by airline ids
     *
     * @return array
     */
    private function groupAirportsByAirline()
    {
        $selectedAirlinesIds = array();
        foreach ($this->selectedAirlines as $airline) {
            $selectedAirlinesIds[] = $airline->getId();
        }
        $groupedAirports = array();
        foreach ($this->selectedAirports as $airport) {
            $airlines = $airport->getCity()->getAirlines();
            foreach ($airlines as $airline) {
                if (in_array($airline->getId(), $selectedAirlinesIds)) {
                    $groupedAirports[$airline->getId()][] = $airport->getId();
                }
            }
        }
        return $groupedAirports;
    }

    /**
     * Set distance between all selected airports
     */
    private function setDistanceBetweenSelectedAirports()
    {
        foreach ($this->selectedAirlines as $airline) {
            $airports = $this->groupedAirports[$airline->getId()];
            $this->distanceBetweenAirports[$airline->getId()] = array();
            foreach ($airports as $sourceId) {
                foreach ($airports as $destinationId) {
                    if ($sourceId === $destinationId) continue;
                    if (isset($this->distanceBetweenAirports[$airline->getId()][$sourceId][$destinationId])) continue;
                    $source = $this->airportRepository->find($sourceId);
                    $destination = $this->airportRepository->find($destinationId);
                    $distance = $this->getDistanceBetweenTwoAirports($source, $destination);
                    $this->distanceBetweenAirports[$airline->getId()][$sourceId][$destinationId] = $distance;
                    $this->distanceBetweenAirports[$airline->getId()][$destinationId][$sourceId] = $distance;
                }
            }
        }
    }

    /**
     * @param Airline $airline
     * @return array|false
     */
    private function generateFlightsForAirline(Airline $airline)
    {
        if ($this->airlineHasEnoughAirports($airline) === false) {
            return false;
        }

        //initialize
        $this->airlineDestinations[$airline->getId()] = array();
        $this->busyAirplanes[$airline->getId()] = array();

        //get a random number of airplanes
        $airplanesPerAirline = rand(self::MIN_AIRPLANES_PER_AIRLINE, self::MAX_AIRPLANES_PER_AIRLINE);
        $airplanes = $airline->getAirplanes()->toArray();
        if (count($airplanes) < $airplanesPerAirline) {
            $airplanesPerAirline = count($airplanes);
        }
        $randKeys = array_rand($airplanes, $airplanesPerAirline);


        foreach ($randKeys as $key) {
            $airplane = $airplanes[$key];
            $flights = $this->generateSetOfFlightsForOneAirplane($airplane);
            if ($flights === false) return false;
            if ($flights) {
                foreach ($flights as $flight) {
                    $this->generatedFlights[] = $flight;
                }
            }
        }
        return true;
    }

    /**
     * Save in database the flights from the class property $generatedFlights
     */
    private function saveTheFlightsInDatabase()
    {
        foreach ($this->generatedFlights as $flight) {
            $this->manager->persist($flight);
        }
        $this->manager->flush();
    }

    /**
     * Calculate the distance in kilometers between two airports using harvesine algorithm
     *
     * @param Airport $airport1
     * @param Airport $airport2
     * @return float
     */
    private function getDistanceBetweenTwoAirports(Airport $airport1, Airport $airport2)
    {
        return $this->formulas->haversineDistanceCalculator($airport1->getLatitude(),
            $airport1->getLongitude(),
            $airport2->getLatitude(),
            $airport2->getLongitude());
    }

    /**
     * Check if for the given airline there were selected at least two airports
     *
     * @param Airline $airline
     * @return bool
     */
    private function airlineHasEnoughAirports(Airline $airline)
    {
        return (isset($this->groupedAirports[$airline->getId()]) &&
            count($this->groupedAirports[$airline->getId()]) > 1);
    }

    /**
     * @param Airplane $airplane
     * @return array|bool
     */
    private function generateSetOfFlightsForOneAirplane(Airplane $airplane)
    {
        if (!isset($this->airplanesReservation[$airplane->getId()])) {
            $this->airplanesReservation[$airplane->getId()] = array();
        }
        $airline = $airplane->getAirline();

        $distanceForAirlineAirports = $this->distanceBetweenAirports[$airline->getId()];
        $airplaneRange = intval($airplane->getType()->getDistance());
        $supportedAirports = $this->getAirportsDistancesForAirplaneRange($distanceForAirlineAirports, $airplaneRange);
        if (empty($supportedAirports)) return false;

        $sourceId = key($supportedAirports);
        $source = $this->airportRepository->find($sourceId);
        $numberOfFlights = rand(self::MIN_FLIGHTS_FOR_AIRPLANE, self::MAX_FLIGHTS_FOR_AIRPLANE);

        $flights = array();
        $count = 0;
        while ($count < $numberOfFlights) {
            $destinations = $supportedAirports[$sourceId];
            $chosenDestination = null;
            $excludedAirportsIds = $this->getExcludedDestinationsForAirline($airplane->getAirline(), $source);
            foreach ($destinations as $id) {
                if (in_array($id, $excludedAirportsIds)) continue;
                $chosenDestination = $id;
            }
            if (is_null($chosenDestination)) return false;

            $destination = $this->airportRepository->find($chosenDestination);
            $flight = $this->generateFlightForAirplane($airplane, $source, $destination);
            $flights[] = $flight;
            $source = $flight->getToAirport();
            $sourceId = $source->getId();
            $count++;
        }
        return $flights;
    }

    /**
     * Return an array of available airports for the plane range
     *
     * @param $airlineAirportsDistances
     * @param $airplaneRange
     * @return array
     */
    private function getAirportsDistancesForAirplaneRange($airlineAirportsDistances, $airplaneRange)
    {
        $supportedAirports = array();
        foreach ($airlineAirportsDistances as $sourceId => $destinations) {
            $supportedAirports[$sourceId] = array();
            foreach ($destinations as $destinationId => $distance) {
                if ($distance < $airplaneRange) {
                    $supportedAirports[$sourceId][] = $destinationId;
                }
            }
        }
        return $supportedAirports;
    }

    /**
     * Get an array of cities ids for which the airline already has the allowed number of flights
     *
     * @param Airline $airline
     * @param Airport|null $exclude
     * @return array
     */
    private function getExcludedDestinationsForAirline(Airline $airline, Airport $exclude = null)
    {
        $excludedIds = array();
        $destinations = $this->airlineDestinations[$airline->getId()];
        $duplicates = array_count_values($destinations);
        foreach ($duplicates as $destination => $frequency) {
            if ($frequency > 3) {
                $excludedIds[] = $destination;
            }
        }
        if ($exclude) {
            array_push($excludedIds, $exclude->getId());
        }
        return $excludedIds;
    }

    /**
     * @param Airplane $airplane
     * @param $source
     * @param $destination
     * @return Flight|null
     */
    private function generateFlightForAirplane(Airplane $airplane, $source, $destination)
    {
        $flight = new Flight();
        $flight->setAirplane($airplane);
        $flight->setAirline($airplane->getAirline());
        $flight->setFromAirport($source);
        $flight->setToAirport($destination);
        $flight = $this->setStandardAttributesForFlight($flight);
        return $flight;
    }

    /**
     * @param Flight $flight
     * @return Flight|null
     */
    private function setStandardAttributesForFlight(Flight $flight)
    {
        $distance = $this->getDistanceBetweenTwoAirports($flight->getFromAirport(), $flight->getToAirport());
        $flight->setDistance($distance);

        $time = $this->formulas->getTime($distance, $flight->getAirplane()->getType()->getSpeed());
        $flight->setDuration($time);

        list($takeOff, $landing) = $this->getAnAvailableIntervalForAirplane($flight->getAirplane(), $time);
        $afterTomorrow = new \DateTime(self::STR_NEXT_DAY_FLIGHTS_GENERATOR);
        if ($takeOff > $afterTomorrow) return null;
        $flight->setTakeOffTime($takeOff);
        $flight->setLandingTime($landing);

        $flight->setPassengers($this->generateRandomPassengersNumber($flight->getAirplane()));

        $this->updateAirlineDestinationArray($flight);
        $this->updateAirplanesReservationArrays($flight);

        return $flight;
    }

    /**
     * @param Airplane $airplane
     * @return int
     */
    private function generateRandomPassengersNumber(Airplane $airplane)
    {
        $capacity = $airplane->getType()->getCapacity();
        return rand($capacity - self::PASSENGERS_MAX_DEVIATION, $capacity);
    }

    /**
     * @param Airplane $airplane
     * @param $duration
     * @return array
     */
    private function getAnAvailableIntervalForAirplane(Airplane $airplane, $duration)
    {
        $airplaneReservation = $this->airplanesReservation[$airplane->getId()];
        if (empty($airplaneReservation)) {
            $timeTakeOff = new \DateTime();
            $timeTakeOff->setTimestamp(strtotime(self::STR_DAY_FLIGHTS_GENERATOR));
        } else {
            $lastInterval = end($airplaneReservation);
            $timeTakeOff = clone $lastInterval['landing'];
            $timeTakeOff->add(new \DateInterval(self::DATEINTERVAL_BETWEEN_AIRPLANE_FLIGHTS));
        }
        $timeLanding = clone $timeTakeOff;
        $timeLanding->add(new \DateInterval("PT" . $duration . "M"));
        return array($timeTakeOff, $timeLanding);
    }

    /**
     * @param Flight $flight
     */
    private function updateAirlineDestinationArray(Flight $flight)
    {
        $this->airlineDestinations[$flight->getAirline()->getId()][] = $flight->getToAirport()->getCity()->getId();
    }

    /**
     * @param Flight $flight
     */
    private function updateAirplanesReservationArrays(Flight $flight)
    {

        array_push($this->airplanesReservation[$flight->getAirplane()->getId()],
            array('landing' => $flight->getLandingTime(),
                'takeOff' => $flight->getTakeOffTime()));
    }

}