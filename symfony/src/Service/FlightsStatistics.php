<?php


namespace App\Service;


use App\Entity\Airline;
use App\Repository\AirlineRepository;
use App\Repository\FlightRepository;

class FlightsStatistics
{
    private $flightRepository;
    private $airlineRepository;
    private $total;
    private $longestFlight;
    private $shortestFlight;
    private $biggestAirline;
    private $smallestAirline;

    public function __construct(FlightRepository $flightRepository, AirlineRepository $airlineRepository)
    {
        $this->flightRepository = $flightRepository;
        $this->airlineRepository = $airlineRepository;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getLongestFlight()
    {
        return $this->longestFlight;
    }

    public function getShortestFlight()
    {
        return $this->shortestFlight;
    }

    public function getBiggestAirline()
    {
        return $this->biggestAirline;
    }

    public function getSmallestAirline()
    {
        return $this->smallestAirline;
    }

    public function getStatistics()
    {
        $this->total = $this->flightRepository->count(array());
        $this->longestFlight = $this->flightRepository->findTheLongestFlight();
        $this->shortestFlight = $this->flightRepository->findTheShortestFlight();
        $this->biggestAirline = $this->airlineRepository->findTopAirline(true);
        $this->smallestAirline = $this->airlineRepository->findTopAirline(false);
        return $this;
    }

}