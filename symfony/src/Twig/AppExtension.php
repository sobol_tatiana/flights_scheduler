<?php


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('format_minutes', array($this, 'convertMinutesToHoursAndMinutes'))
        );
    }

    public function convertMinutesToHoursAndMinutes($minutes)
    {
        return sprintf("%2dh %02dm", floor($minutes / 60), $minutes % 60);
    }

}