<?php

namespace App\Repository;

use App\Entity\Airline;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Airline|null find($id, $lockMode = null, $lockVersion = null)
 * @method Airline|null findOneBy(array $criteria, array $orderBy = null)
 * @method Airline[]    findAll()
 * @method Airline[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AirlineRepository extends ServiceEntityRepository
{
    /**
     * AirlineRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Airline::class);
    }

    /**
     * Find the top airplane by the number of planes and passengers
     *
     * @param bool $biggest
     * @return mixed
     */
    public function findTopAirline($biggest = true)
    {
        $sql = $this->getSqlForTopAirline($biggest);
        $query = $this->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        return $result ? $result[0] : false;
    }

    /**
     * @param bool $biggest
     * @return string
     */
    private function getSqlForTopAirline($biggest = true)
    {
        $order = $biggest === true ? "DESC" : "ASC";

        $sql = "SELECT
    q1.id,
    q1.name,
    planes,
    passengers
FROM
    (
         SELECT
            COUNT(*) AS planes,
            a.id,
            a.name
        FROM
            airplane
        INNER JOIN
            airline a
        ON
            a .id = airline_id
        GROUP BY
            airline_id
        ORDER BY
            planes DESC ) AS q1
INNER JOIN
    (
        SELECT
            a.id,
            SUM(f.passengers) AS passengers
        FROM
            airline a
        INNER JOIN
            flight f
        ON
            a.id=f.airline_id
        GROUP BY
            a.id) q2
ON
    q1.id=q2.id
ORDER BY
    planes $order,
    passengers $order
LIMIT 1";
        return $sql;
    }

    public function findRandomAirlines($limit)
    {
        $airplanes = array();
        $sql = "SELECT id FROM airline ORDER BY RAND() Limit $limit";
        $query = $this->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        foreach ($result as $row) {
            $airplanes[] = $this->find($row['id']);
        }
        return $airplanes;
    }

}
