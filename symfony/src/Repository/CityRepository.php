<?php

namespace App\Repository;

use App\Entity\City;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    /**
     * CityRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, City::class);
    }

    /**
     * @param int $limit
     * @return \mixed[] array of cities ids
     */
    public function findTheMostOperated($limit =10)
    {
        $sql = "SELECT
    city_id id
FROM
    (
        SELECT
            COUNT(city_id) AS c,
            city_id
        FROM
            airline_city
        GROUP BY
            city_id
        ORDER BY
            c DESC) q LIMIT $limit";
        $query = $this->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * @param int $limit
     * @return \mixed[] array of cities ids
     */
    public function findTheMostOperatedForAirlines($limit = 10, $airlines)
    {
        $ids = '';
        foreach($airlines as $airline){
            $ids[]= $airline->id;
        }
        $inCondition = implode($ids);

        $sql = "SELECT
    city_id id
FROM
    (
        SELECT
            COUNT(city_id) AS c,
            city_id
        FROM
            airline_city
        GROUP BY
            city_id
        AND airline_id IN ($inCondition)
        ORDER BY
            c DESC) q LIMIT $limit";
        $query = $this->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
}
