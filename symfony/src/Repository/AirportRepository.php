<?php

namespace App\Repository;

use App\Entity\Airport;
use App\Entity\City;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Airport|null find($id, $lockMode = null, $lockVersion = null)
 * @method Airport|null findOneBy(array $criteria, array $orderBy = null)
 * @method Airport[]    findAll()
 * @method Airport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AirportRepository extends ServiceEntityRepository
{
    /**
     * AirportRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Airport::class);
    }

    /**
     * @param $citiesIds
     * @param int $limit
     * @return mixed
     */
    public function findAllByCitiesIds($citiesIds, $limit = 10)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.city IN(:val)')
            ->setParameter('val', array_values($citiesIds))
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $airportsIds
     * @return mixed
     */
    public function findAllExcept($airportsIds)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.id NOT IN(:val)')
            ->setParameter('val', array_values($airportsIds))
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $limit
     * @return mixed
     */
    public function findTheMostOperated($limit)
    {
        $citiesIds = $this->getEntityManager()->getRepository(City::class)->findTheMostOperated($limit);
        return $this->findAllByCitiesIds($citiesIds, $limit);
    }

    /**
     * @param $limit
     * @return mixed
     */
    public function findTheMostOperatedForAirlines($limit, $airlines)
    {
        $citiesIds = $this->getEntityManager()->getRepository(City::class)->findTheMostOperated($limit);
        return $this->findAllByCitiesIds($citiesIds, $limit);
    }

    public function findForAjax($limit, $offset)
    {
        return $this->createQueryBuilder('a')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function countAll(){
        return $this->createQueryBuilder('a')
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
