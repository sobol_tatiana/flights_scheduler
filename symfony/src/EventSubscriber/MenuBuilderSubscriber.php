<?php

namespace App\EventSubscriber;

use KevinPapst\AdminLTEBundle\Event\SidebarMenuEvent;
use KevinPapst\AdminLTEBundle\Event\ThemeEvents;
use KevinPapst\AdminLTEBundle\Model\MenuItemModel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MenuBuilderSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            ThemeEvents::THEME_SIDEBAR_SETUP_MENU => ['onSetupMenu', 100],
        ];
    }

    public function onSetupMenu(SidebarMenuEvent $event)
    {
        $flightSection = new MenuItemModel('flightSection', 'Flights', 'flight_list', [], 'fas fa-paper-plane');
        $flightSection->addChild(
            new MenuItemModel('allFlights', 'List flights', 'flight_list', [], 'fa fa-circle')
        )->addChild(
            new MenuItemModel('generate', 'Generate', 'flights_generation_index', [], 'fa fa-circle' )
        );
        $event->addItem($flightSection);

        $airlineSection = new MenuItemModel('airlineSection', 'Airlines', 'airline_index', [], 'fas fa-grip-lines');
        $airlineSection->addChild(
            new MenuItemModel('allAirlines', 'All airlines', 'airline_index', [], 'fa fa-circle')
        )->addChild(
            new MenuItemModel('newAirline', 'New airline', 'airline_new', [], 'fa fa-circle')
        )->addChild(
            new MenuItemModel('addAirplanes', 'Add airplanes', 'airline_link', [], 'fa fa-circle')
        );
        $event->addItem($airlineSection);

        $airplaneSection = new MenuItemModel('airplaneSection', 'Airplanes', 'airplane_index', [], 'fas fa-plane');
        $airplaneSection->addChild(
            new MenuItemModel('allAirplanes', 'All airplanes', 'airplane_index', [], 'fa fa-circle')
        )->addChild(
            new MenuItemModel('newAirline', 'New airplane', 'airplane_new', [], 'fa fa-circle')
        );
        $event->addItem($airplaneSection);

        $airportSection = new MenuItemModel('airportSection', 'Airports', 'airport_index', [], 'fas fa-broadcast-tower');
        $airportSection->addChild(
            new MenuItemModel('allAirports', 'All airports', 'airport_index', [], 'fa fa-circle')
        )->addChild(
            new MenuItemModel('newAirport', 'New airport', 'airport_new', [], 'fa fa-circle')
        );
        $event->addItem($airportSection);

        $this->activateByRoute(
            $event->getRequest()->get('_route'),
            $event->getItems()
        );
    }

    /**
     * @param string $route
     * @param MenuItemModel[] $items
     */
    protected function activateByRoute($route, $items)
    {
        foreach ($items as $item) {
            if ($item->hasChildren()) {
                $this->activateByRoute($route, $item->getChildren());
            } elseif ($item->getRoute() == $route) {
                $item->setIsActive(true);
            }
        }
    }
}