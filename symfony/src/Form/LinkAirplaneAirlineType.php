<?php


namespace App\Form;

use App\Entity\Airline;
use App\Entity\AirplaneModel;
use App\Repository\AirlineRepository;
use App\Repository\AirplaneModelRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LinkAirplaneAirlineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('airline', EntityType::class, array(
                'class' => Airline::class,
                'query_builder' => function (AirlineRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->orderBy('a.name', 'ASC');
                },
                'choice_label' => 'name',
            ))
            ->add('airplane', EntityType::class, array(
                'class' => AirplaneModel::class,
                'query_builder' => function (AirplaneModelRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->orderBy('a.name', 'ASC');
                },
                'choice_label' => 'name',
            ))
            ->add('number', IntegerType::class, array(
                'label' => 'Number of airplanes',
                'data' => 1))
            ->add('submit', SubmitType::class, array('label' => 'Save'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }

}