<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AirplaneModelRepository")
 * @ApiResource
 */
class AirplaneModel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * Maximum range in kilometers
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $distance;

    /**
     * Number of seats
     * @ORM\Column(type="smallint")
     */
    private $capacity;

    /**
     * km/h
     * @ORM\Column(type="decimal", precision=7, scale=3)
     */
    private $speed;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Airplane", mappedBy="type", orphanRemoval=true)
     */
    private $airplanes;

    public function __construct()
    {
        $this->airplanes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDistance()
    {
        return $this->distance;
    }

    public function setDistance($distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    public function setSpeed($speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * @return Collection|Airplane[]
     */
    public function getAirplanes(): Collection
    {
        return $this->airplanes;
    }

    public function addAirplane(Airplane $airplane): self
    {
        if (!$this->airplanes->contains($airplane)) {
            $this->airplanes[] = $airplane;
            $airplane->setType($this);
        }

        return $this;
    }

    public function removeAirplane(Airplane $airplane): self
    {
        if ($this->airplanes->contains($airplane)) {
            $this->airplanes->removeElement($airplane);
            // set the owning side to null (unless already changed)
            if ($airplane->getType() === $this) {
                $airplane->setType(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
       return $this->getName();
    }
}
