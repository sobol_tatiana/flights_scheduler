<?php

namespace App\Entity;

use App\Repository\PossessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AirlineRepository")
 * @ApiResource(attributes={"order"={"name": "ASC"}})
 *
 */
class Airline
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiFilter(SearchFilter::class, strategy="iword_start")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country", inversedBy="airlines")
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource
     */
    private $country;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\City", inversedBy="airlines")
     */
    private $cities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Flight", mappedBy="airline", orphanRemoval=true)
     */
    private $flights;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Airplane", mappedBy="airline", orphanRemoval=true)
     */
    private $airplanes;

    public function __construct()
    {
        $this->cities = new ArrayCollection();
        $this->flights = new ArrayCollection();
        $this->airplanes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|City[]
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }

    public function addCity(City $city): self
    {
        if (!$this->cities->contains($city)) {
            $this->cities[] = $city;
        }

        return $this;
    }

    public function removeCity(City $city): self
    {
        if ($this->cities->contains($city)) {
            $this->cities->removeElement($city);
        }

        return $this;
    }

    /**
     * @return Collection|Flight[]
     */
    public function getFlights(): Collection
    {
        return $this->flights;
    }

    public function addFlight(Flight $flight): self
    {
        if (!$this->flights->contains($flight)) {
            $this->flights[] = $flight;
            $flight->setAirline($this);
        }

        return $this;
    }

    public function removeFlight(Flight $flight): self
    {
        if ($this->flights->contains($flight)) {
            $this->flights->removeElement($flight);
            // set the owning side to null (unless already changed)
            if ($flight->getAirline() === $this) {
                $flight->setAirline(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Airplane[]
     */
    public function getAirplanes(): Collection
    {
        return $this->airplanes;
    }

    public function addAirplane(Airplane $airplane): self
    {
        if (!$this->airplanes->contains($airplane)) {
            $this->airplanes[] = $airplane;
            $airplane->setAirline($this);
        }

        return $this;
    }

    public function removeAirplane(Airplane $airplane): self
    {
        if ($this->airplanes->contains($airplane)) {
            $this->airplanes->removeElement($airplane);
            // set the owning side to null (unless already changed)
            if ($airplane->getAirline() === $this) {
                $airplane->setAirline(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
