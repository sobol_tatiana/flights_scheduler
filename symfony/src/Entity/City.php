<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CityRepository")
 * @ApiResource
 */
class City
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country", inversedBy="cities")
     * @ORM\JoinColumn(nullable=false)
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Airport", mappedBy="city")
     */
    private $airports;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Airline", mappedBy="cities")
     */
    private $airlines;

    public function __construct()
    {
        $this->airports = new ArrayCollection();
        $this->airlines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Airport[]
     */
    public function getAirports(): Collection
    {
        return $this->airports;
    }

    public function addAirport(Airport $airport): self
    {
        if (!$this->airports->contains($airport)) {
            $this->airports[] = $airport;
            $airport->setCity($this);
        }

        return $this;
    }

    public function removeAirport(Airport $airport): self
    {
        if ($this->airports->contains($airport)) {
            $this->airports->removeElement($airport);
            // set the owning side to null (unless already changed)
            if ($airport->getCity() === $this) {
                $airport->setCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Airline[]
     */
    public function getAirlines(): Collection
    {
        return $this->airlines;
    }

    public function addAirline(Airline $airline): self
    {
        if (!$this->airlines->contains($airline)) {
            $this->airlines[] = $airline;
            $airline->addCity($this);
        }

        return $this;
    }

    public function removeAirline(Airline $airline): self
    {
        if ($this->airlines->contains($airline)) {
            $this->airlines->removeElement($airline);
            $airline->removeCity($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
