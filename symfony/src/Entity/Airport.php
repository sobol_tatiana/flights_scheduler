<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AirportRepository")
 * @ApiResource
 */
class Airport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=8)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=11, scale=8)
     */
    private $longitude;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $openTime;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $closeTime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="airports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Flight", mappedBy="fromAirport", orphanRemoval=true)
     */
    private $outgoingFlights;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Flight", mappedBy="toAirport", orphanRemoval=true)
     */
    private $incomingflights;

    public function __construct()
    {
        $this->outgoingFlights = new ArrayCollection();
        $this->incomingflights = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getOpenTime(): ?\DateTimeInterface
    {
        return $this->openTime;
    }

    public function setOpenTime(?\DateTimeInterface $openTime): self
    {
        $this->openTime = $openTime;

        return $this;
    }

    public function getCloseTime(): ?\DateTimeInterface
    {
        return $this->closeTime;
    }

    public function setCloseTime(?\DateTimeInterface $closeTime): self
    {
        $this->closeTime = $closeTime;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection|Flight[]
     */
    public function getOutgoingFlights(): Collection
    {
        return $this->outgoingFlights;
    }

    public function addOutgoingFlight(Flight $outgoingFlight): self
    {
        if (!$this->outgoingFlights->contains($outgoingFlight)) {
            $this->outgoingFlights[] = $outgoingFlight;
            $outgoingFlight->setFromAirport($this);
        }

        return $this;
    }

    public function removeOutgoingFlight(Flight $outgoingFlight): self
    {
        if ($this->outgoingFlights->contains($outgoingFlight)) {
            $this->outgoingFlights->removeElement($outgoingFlight);
            // set the owning side to null (unless already changed)
            if ($outgoingFlight->getFromAirport() === $this) {
                $outgoingFlight->setFromAirport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Flight[]
     */
    public function getIncomingflights(): Collection
    {
        return $this->incomingflights;
    }

    public function addIncomingflight(Flight $incomingflight): self
    {
        if (!$this->incomingflights->contains($incomingflight)) {
            $this->incomingflights[] = $incomingflight;
            $incomingflight->setToAirport($this);
        }

        return $this;
    }

    public function removeIncomingflight(Flight $incomingflight): self
    {
        if ($this->incomingflights->contains($incomingflight)) {
            $this->incomingflights->removeElement($incomingflight);
            // set the owning side to null (unless already changed)
            if ($incomingflight->getToAirport() === $this) {
                $incomingflight->setToAirport(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->name. " City:". $this->getCity();
    }
}
