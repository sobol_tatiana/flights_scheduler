<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FlightRepository")
 * @ApiResource
 */
class Flight
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $takeOffTime;

    /**
     * @ORM\Column(type="datetime")
     */
    private $landingTime;

    /**
     * @ORM\Column(type="integer")
     */
    private $passengers;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duration;

    /**
     * Flight distance in kilometers
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $distance;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Airport", inversedBy="outgoingFlights")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fromAirport;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Airport", inversedBy="incomingflights")
     * @ORM\JoinColumn(nullable=false)
     */
    private $toAirport;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Airplane")
     * @ORM\JoinColumn(nullable=false)
     */
    private $airplane;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Airline", inversedBy="flights")
     * @ORM\JoinColumn(nullable=false)
     */
    private $airline;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTakeOffTime(): ?\DateTimeInterface
    {
        return $this->takeOffTime;
    }

    public function setTakeOffTime(\DateTimeInterface $takeOffTime): self
    {
        $this->takeOffTime = $takeOffTime;

        return $this;
    }

    public function getLandingTime(): ?\DateTimeInterface
    {
        return $this->landingTime;
    }

    public function setLandingTime(\DateTimeInterface $landingTime): self
    {
        $this->landingTime = $landingTime;

        return $this;
    }

    public function getPassengers(): ?int
    {
        return $this->passengers;
    }

    public function setPassengers(int $passengers): self
    {
        $this->passengers = $passengers;

        return $this;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getDistance()
    {
        return $this->distance;
    }

    public function setDistance($distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getFromAirport(): ?Airport
    {
        return $this->fromAirport;
    }

    public function setFromAirport(?Airport $fromAirport): self
    {
        $this->fromAirport = $fromAirport;

        return $this;
    }

    public function getToAirport(): ?Airport
    {
        return $this->toAirport;
    }

    public function setToAirport(?Airport $toAirport): self
    {
        $this->toAirport = $toAirport;

        return $this;
    }

    public function getAirplane(): ?Airplane
    {
        return $this->airplane;
    }

    public function setAirplane(?Airplane $airplane): self
    {
        $this->airplane = $airplane;

        return $this;
    }

    public function getAirline(): ?Airline
    {
        return $this->airline;
    }

    public function setAirline(?Airline $airline): self
    {
        $this->airline = $airline;

        return $this;
    }

    public function __toString()
    {
        return $this->getId() . " From: " . $this->getFromAirport()
            . " To: " .$this->getToAirport()
            . " Airplane:". $this->getAirplane()
            . " Take off time:". $this->getTakeOffTime()->format('Y-m-d H:i:s')
            . " Landing Time:". $this->getLandingTime()->format('Y-m-d H:i:s')
            . " Duration ". $this->getDuration()
            . " Distance: ". $this->getDistance();
    }
}
