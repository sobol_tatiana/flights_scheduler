<?php

namespace App\Controller;

use App\Entity\Airline;
use App\Entity\Airplane;
use App\Form\AirlineType;
use App\Form\LinkAirplaneAirlineType;
use App\Repository\AirlineRepository;
use App\Service\LinkService;
use Fig\Link\Link;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/airline")
 */
class AirlineController extends AbstractController
{
    /**
     * @Route("/", name="airline_index", methods="GET")
     */
    public function index(AirlineRepository $airlineRepository): Response
    {
        return $this->render('airline/index.html.twig', ['airlines' => $airlineRepository->findAll()]);
    }

    /**
     * @Route("/new", name="airline_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $airline = new Airline();
        $form = $this->createForm(AirlineType::class, $airline);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($airline);
            $em->flush();

            return $this->redirectToRoute('airline_index');
        }

        return $this->render('airline/new.html.twig', [
            'airline' => $airline,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="airline_show", methods="GET", requirements={"id"="\d+"})
     */
    public function show(Airline $airline): Response
    {
        $airplanes = $airline->getAirplanes();
        $result = array();

        //TODO: add function in  service
        foreach($airplanes as $airplane){
            $type = $airplane->getType();
            $result[$type->getId()]['name'] = $type->getName();
            if(isset($result[$type->getId()]['number'])){
                $result[$type->getId()]['number']++;
            } else {
                $result[$type->getId()]['number'] = 1;
            }
        }
        return $this->render('airline/show.html.twig', ['airline' => $airline, 'airplanes' => $result]);
    }

    /**
     * @Route("/{id}/edit", name="airline_edit", methods="GET|POST", requirements={"id"="\d+"})
     */
    public function edit(Request $request, Airline $airline): Response
    {
        $form = $this->createForm(AirlineType::class, $airline);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('airline_index', ['id' => $airline->getId()]);
        }

        return $this->render('airline/edit.html.twig', [
            'airline' => $airline,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="airline_delete", methods="DELETE", requirements={"id"="\d+"})
     */
    public function delete(Request $request, Airline $airline): Response
    {
        if ($this->isCsrfTokenValid('delete'.$airline->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($airline);
            $em->flush();
        }

        return $this->redirectToRoute('airline_index');
    }

    /**
     * @Route("/link", name="airline_link", methods="GET|POST")
     */
    public function addAirplanes(Request $request, LinkService $link)  : Response
    {
        $form = $this->createForm(LinkAirplaneAirlineType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $link->addAirplanesToAirline($data['airline'], $data['airplane'], $data['number']);
            //redirect to this airline
            return $this->redirectToRoute('airline_show', array('id' => $data['airline']->getId()));
        }

        return $this->render('airline/link.html.twig', [
            'form' => $form->createView()
        ]);

    }

}
