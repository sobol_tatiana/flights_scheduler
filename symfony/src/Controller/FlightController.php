<?php

namespace App\Controller;

use App\Entity\Flight;
use App\Service\FlightsGenerator;
use App\Service\FlightsStatistics;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class FlightController extends AbstractController
{
    /**
     * @Route("/", name="home")
     *
     * @param FlightsStatistics $flightsStatistics
     * @return Response
     */
    public function statistics(FlightsStatistics $flightsStatistics)
    {
        $statistics = $flightsStatistics->getStatistics();
        return $this->render('flight/statistics.html.twig', [
            'total' => $statistics->getTotal(),
            'longestFlight' => $statistics->getLongestFlight(),
            'shortestFlight' => $statistics->getShortestFlight(),
            'biggestAirline' => $statistics->getBiggestAirline(),
            'smallestAirline' => $statistics->getSmallestAirline()
        ]);
    }

    /**
     * @Route("/flight", name="flight_list")
     */
    public function list()
    {
        $flights = $this->getDoctrine()->getRepository(Flight::class)->findAll();
        return $this->render('flight/index.html.twig', [
            'flights' => $flights,
        ]);
    }

    /**
     * @Route("/flight/generate", name="flights_generation_index")
     */
    public function generationIndex()
    {
        return $this->render('flight/generate.html.twig');
    }


    /**
     * @Route("/flight/generate/go", name="flights_generation")
     *
     * @param FlightsGenerator $flightsGenerator
     * @return JsonResponse
     */
    public function generate(FlightsGenerator $flightsGenerator)
    {
        $flights = $flightsGenerator->generate();
        $data = array('count' => count($flights));
        return new JsonResponse($data);
    }
}
