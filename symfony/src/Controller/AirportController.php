<?php

namespace App\Controller;

use App\Entity\Airport;
use App\Form\AirportType;
use App\Repository\AirportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/airport")
 */
class AirportController extends AbstractController
{
    /**
     * @Route("/", name="airport_index", methods="GET")
     */
    public function index(AirportRepository $airportRepository): Response
    {
        return $this->render('airport/index.html.twig', ['airports' => $airportRepository->findAll()]);
    }

    /**
     * @Route("/new", name="airport_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $airport = new Airport();
        $form = $this->createForm(AirportType::class, $airport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($airport);
            $em->flush();

            return $this->redirectToRoute('airport_index');
        }

        return $this->render('airport/new.html.twig', [
            'airport' => $airport,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="airport_show", methods="GET", requirements={"id"="\d+"})
     */
    public function show(Airport $airport): Response
    {
        return $this->render('airport/show.html.twig', ['airport' => $airport]);
    }

    /**
     * @Route("/{id}/edit", name="airport_edit", methods="GET|POST")
     */
    public function edit(Request $request, Airport $airport): Response
    {
        $form = $this->createForm(AirportType::class, $airport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('airport_index', ['id' => $airport->getId()]);
        }

        return $this->render('airport/edit.html.twig', [
            'airport' => $airport,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="airport_delete", methods="DELETE", requirements={"id"="\d+"})
     */
    public function delete(Request $request, Airport $airport): Response
    {
        if ($this->isCsrfTokenValid('delete'.$airport->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($airport);
            $em->flush();
        }

        return $this->redirectToRoute('airport_index');
    }


    /**
     * @Route("/ajax", name="airport_list_ajax")
     */
    public function ajaxData(Request $request)
    {
        $offset = $request->query->get('start') ? $request->query->get('start') :  0 ;
        $limit = $request->query->get('length') ? $request->query->get('length') : 10 ;
        $draw = $request->query->get('draw')  ? $request->query->get('draw') : 0;

        $airports = $this->getDoctrine()->getRepository(Airport::class)->findForAjax($limit, $offset);
        $data = array();
        foreach($airports as $airport){
            $record["id"] = $airport->getId();
            $record["name"] = $airport->getName();
            $record["latitude"] = number_format($airport->getLatitude(), 1);
            $record["longitude"] = number_format($airport->getLongitude(), 1);
            $record["openTime"] = $airport->getOpenTime();
            $record["closeTime"] = $airport->getCloseTime();
            $data['data'][]=$record;
        }
        $total =  $this->getDoctrine()->getRepository(Airport::class)->countAll();
        $data['recordsTotal'] = $total;
        $data['recordsFiltered'] = $total;
        $data['draw'] =   $draw;
        return new JsonResponse($data);
    }
}
