<?php

namespace App\Controller;

use App\Entity\AirplaneModel;
use App\Form\AirplaneModelType;
use App\Repository\AirplaneModelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/airplane")
 */
class AirplaneModelController extends AbstractController
{
    /**
     * @Route("/", name="airplane_index", methods="GET")
     */
    public function index(AirplaneModelRepository $airplaneRepository): Response
    {
        return $this->render('airplane/index.html.twig', ['airplanes' => $airplaneRepository->findAll()]);
    }

    /**
     * @Route("/new", name="airplane_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $airplane = new AirplaneModel();
        $form = $this->createForm(AirplaneModelType::class, $airplane);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($airplane);
            $em->flush();

            return $this->redirectToRoute('airplane_index');
        }

        return $this->render('airplane/new.html.twig', [
            'airplane' => $airplane,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="airplane_show", methods="GET")
     */
    public function show(AirplaneModel $airplane): Response
    {
        return $this->render('airplane/show.html.twig', ['airplane' => $airplane]);
    }

    /**
     * @Route("/{id}/edit", name="airplane_edit", methods="GET|POST")
     */
    public function edit(Request $request, AirplaneModel $airplane): Response
    {
        $form = $this->createForm(AirplaneModelType::class, $airplane);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('airplane_index', ['id' => $airplane->getId()]);
        }

        return $this->render('airplane/edit.html.twig', [
            'airplane' => $airplane,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="airplane_delete", methods="DELETE")
     */
    public function delete(Request $request, AirplaneModel $airplane): Response
    {
        if ($this->isCsrfTokenValid('delete'.$airplane->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($airplane);
            $em->flush();
        }

        return $this->redirectToRoute('airplane_index');
    }
}
